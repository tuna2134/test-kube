use k8s_openapi::api::core::v1::{Container, Pod, PodSpec};
use k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta;
use kube::{
    api::{Api, ListParams, PostParams, ResourceExt},
    Client,
};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let client = Client::try_default().await?;
    let pods: Api<Pod> = Api::default_namespaced(client);

    for p in pods.list(&ListParams::default()).await? {
        println!("found pod {}", p.name_any());
    }

    // Create pod
    let pod = Pod {
        metadata: ObjectMeta {
            name: Some(String::from("nginx")),
            ..Default::default()
        },
        spec: Some(PodSpec {
            containers: vec![Container {
                name: String::from("nginx"),
                image: Some(String::from("nginx")),
                ..Default::default()
            }],
            ..Default::default()
        }),
        ..Default::default()
    };
    pods.create(&PostParams::default(), &pod).await?;

    Ok(())
}
